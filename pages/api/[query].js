import google from 'googlethis';

export default async function handler(req, res){
    const options = {
        page: 0,
        safe: false,
        parse_ads: false,
        additional_params: {
            hl: 'en'
        }
    }
    google.search(req.query["query"], options).then((response) => res.status(200).json(response.results)).catch(error => res.status(500).json(error));
}